<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
            ['title'=>'Poemas de amor', 'description'=>''],
            ['title'=>'Canto General', 'description'=>''],
            ['title'=>'Libro de las preguntas', 'description'=>''],
            ['title'=>'100 años de soledad', 'description'=>''],
        ];

        DB::table('books')->insert($books);
    }
}
