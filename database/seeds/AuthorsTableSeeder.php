<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            ['name'=>'Pablo Neruda'],
            ['name'=>'García Marquez'],
        ];

        DB::table('authors')->insert($authors);

    }
}
