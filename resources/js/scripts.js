document.addEventListener('DOMContentLoaded', () => {

    (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
        $notification = $delete.parentNode;
        $delete.addEventListener('click', () => {
            $notification.parentNode.removeChild($notification);
        });
    });

    (document.querySelectorAll('.message .delete') || []).forEach(($delete) => {
        $message = $delete.parentNode;
        $delete.addEventListener('click', () => {
            $message.parentNode.remove($message);
        });
    });

});