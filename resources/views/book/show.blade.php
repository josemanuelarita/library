@extends('layouts.default')

@section('title', 'Libro ' . $book->title)

@section('content')

<div class="card">
    <header class="card-header">
        <p class="card-header-title">
            Libro {{ $book->title }}
        </p>
    </header>
    <div class="card-content">
        <div class="content">
                @if (empty($book->description))
                    No hay descripción
                @else
                    {{ $book->description }}
                @endif
        </div>
        <div class="level">
            <div class="level-left">
               <form action="{{ route('books.destroy', $book->id) }}" class="form" method="post">
                @method('DELETE')
                @csrf
                   <div class="field">
                       <div class="control">
                           <button class="button is-danger">Borrar</button>
                        </div>
                   </div>
               </form>
            </div>
            <div class="level-right">
                <a href="{{ route('books.edit', $book->id) }}" class="button is-info">Editar</a>
            </div>
        </div>
    </div>
</div>


@endsection