@extends('layouts.default')

@section('title', 'Todos los libros')

@section('content')
<h1 class="is-1">Books</h1>
<table class="table">
    <thead>
        <tr>
            <th>Título</th>
            <th>Autores</th>
            <th>Detalle</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($books as $book)
        <tr>
            <td>{{ $book->title }}</td>
            <td>
                @foreach ($book->authors as $author)
                {{ $author->name }},
                @endforeach
            </td>
            <td>
                <a href="{{ route('books.show', $book->id) }}" class="link">ver</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection