@extends('layouts.default')

@section('content')

<div class="card">
    <div class="card-header">
        <header class="header">
            <p class="card-header-title">
                @if ($edited)
                    @section('title', 'Editar ' . $book->title)
                    Editar {{ $book->title }}
                @else
                    @section('title', 'Agregar nuevo libro')
                    Nuevo Libro
                @endif
            </p>
        </header>
    </div>
    <div class="card-content">
        <form
        @if ($edited)
            action="{{ route('books.update', $book->id) }}"
            @else
        action="{{ route('books.store') }}"
        @endif
            method="post" class="form">
            @csrf
            @if ($edited)
                @method('PATCH')
            @endif
            <div class="field">
                <div class="control">
                    <input type="text" name="title" value="{{ $book->title ?? ''}}" class="input">
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <input value="{{ $book->description ?? '' }}" class="textarea" name="description">
                </div>
            </div>
            <div class="level">
                <div class="level-left">
                    <a href="#" class="button">Cancelar</a>
                </div>
                <div class="level-right">
                    <button class="button">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection