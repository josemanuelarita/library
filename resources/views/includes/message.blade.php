@if ($message = Session::get('success'))
<div class="notification is-success">
    <button class="delete"></button>
    {{ $message }}
</div>
@endif

@if ($message = Session::get('error'))
<div class="notification is-danger">
    <button class="delete"></button>
    {{ $message }}
</div>
@endif


@if ($message = Session::get('warning'))
<div class="notification is-warning">
    <button class="delete"></button>
    {{ $message }}
</div>
@endif


@if ($message = Session::get('info'))
<div class="notification is-info">
    <button class="delete"></button>
    {{ $message }}
</div>
@endif

@if ($errors->any())
<article class="message is-danger">
    <div class="message-header">
        <p>@lang('message.data_error')</p>
        <button class="delete" aria-label="delete"></button>
    </div>
    <div class="message-body">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</article>
@endif