<?php

return [
    'data_error' => 'You have some errors.',
    'created' => 'Created successfully',
    'edited' => 'Edited successfully',
    'deleted' => 'Deleted successfully',
];