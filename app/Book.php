<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'description'];

    public function authors(){
        return $this->belongsToMany('App\Author');
    }
}
